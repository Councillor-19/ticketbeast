<?php

namespace Tests\Feature;

use App\Concert;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\DuskTestCase;
use Laravel\Dusk\Chrome;
use Tests\TestCase;

class ViewContentListingTest extends TestCase
{

    use DatabaseMigrations;

    /**
     * @test
     *
     * @return void
     */
    public function user_can_view_a_published_concert_listing()
    {
        $concert = factory(Concert::class)->states('published')->create([
            'title' => 'The Red Chord',
            'subtitle' => 'with Animosity and Lethargy',
            'date' => Carbon::parse('December 13, 2006 8:00pm'),
            'ticket_price' => 3250,
            'venue' => 'The Mosh Pit',
            'venue_address' => '123 Example Name',
            'city' => 'Laraville',
            'state' => 'ON',
            'zip' => '17654',
            'additional_information' => 'For tickets, call (555) 555-5555.',
        ]);

        $response = $this->get('/concerts/'. $concert->id);

        $response->assertSee('The Red Chord');
        $response->assertSee('with Animosity and Lethargy');
        $response->assertSee('December 13, 2006');
        $response->assertSee('Doors at 8:00pm');
        $response->assertSee('32.50');
        $response->assertSee('The Mosh Pit');
        $response->assertSee('123 Example Name');
        $response->assertSee('Laraville, ON 17654');
        $response->assertSee('For tickets, call (555) 555-5555.');
    }

    /**
     * @test
     */
    public function user_cannot_view_unpublished_listings()
    {
        $concert = factory(Concert::class)->states('unpublished')->create();

        $response = $this->get('/concerts/'. $concert->id);

        $response->assertStatus(404);
    }
}
